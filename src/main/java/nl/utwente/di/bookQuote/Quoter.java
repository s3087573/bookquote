package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> Prices;
    public double getBookPrice(String isbn){
        Prices = new HashMap<> ();
        Prices.put ("1", 10.0);
        Prices.put ("2", 45.0);
        Prices.put ("3", 20.0);
        Prices.put ("4", 35.0);
        Prices.put ("5", 50.0);
        if(Prices.containsKey (isbn)) {
            return Prices.get (isbn);
        }else{
            return 0.0;
        }
    }
}
